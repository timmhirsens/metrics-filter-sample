package com.example.metricsfilter;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class MetricsFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetricsFilterApplication.class, args);
	}

	@Bean
	FilterRegistrationBean<UnsichtbarFilter> filter() {
		FilterRegistrationBean<UnsichtbarFilter> registration = new FilterRegistrationBean<>();
		registration.setName("Unsichtbar Filter");
		registration.setUrlPatterns(Collections.singletonList("/unsichtbar/*"));
		registration.setFilter(new UnsichtbarFilter());
		return registration;
	}

	@RestController
	class ControllerRest {

		@GetMapping("/sichtbar")
		public ResponseEntity<String> sichtbar() {
			return ResponseEntity.ok("Sichtbar");
		}

		@GetMapping("/unsichtbar")
		public ResponseEntity<String> unsichtbar() {
			return ResponseEntity.ok("Unsichtbar");
		}

	}

	class UnsichtbarFilter implements Filter {

		@Override
		public void init(FilterConfig filterConfig) throws ServletException {

		}

		@Override
		public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
			((HttpServletResponse) response).sendError(401, "Verboten");
		}

		@Override
		public void destroy() {
		}

	}

}
